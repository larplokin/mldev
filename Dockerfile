FROM python:3.8-slim-bookworm

WORKDIR /app

COPY . .

RUN apt-get update && apt-get install -y jq psmisc build-essential git git-lfs curl unzip zip lsof

RUN echo "n n" | ./install_mldev.sh

RUN pip install -r test/requirements.txt

CMD ["pytest"]
