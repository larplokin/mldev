\babel@toc {russian}{}\relax 
\contentsline {chapter}{\numberline {1}Quick setup}{3}{chapter.1}%
\contentsline {chapter}{\numberline {2}User documentation}{5}{chapter.2}%
\contentsline {section}{\numberline {2.1}A Quickstart Tutorial for MLDev}{5}{section.2.1}%
\contentsline {subsection}{\numberline {2.1.1}Contents}{5}{subsection.2.1.1}%
\contentsline {subsection}{\numberline {2.1.2}Installation instructions}{6}{subsection.2.1.2}%
\contentsline {subsection}{\numberline {2.1.3}Hello World!}{6}{subsection.2.1.3}%
\contentsline {subsubsection}{Step 1. Create a pipeline}{6}{subsubsection*.3}%
\contentsline {subsubsection}{Step 2. Add a script to the stage hello\_world}{6}{subsubsection*.4}%
\contentsline {subsubsection}{Step 3. Setup and run the experiment}{7}{subsubsection*.5}%
\contentsline {subsubsection}{Hello World completed!}{7}{subsubsection*.6}%
\contentsline {subsection}{\numberline {2.1.4}Simple classification task on template\sphinxhyphen {}default}{8}{subsection.2.1.4}%
\contentsline {subsubsection}{Step 1. Get the template}{8}{subsubsection*.7}%
\contentsline {subsubsection}{Step 2. Prepare the data}{8}{subsubsection*.8}%
\contentsline {subsubsection}{Step 3. Two stage pipeline}{9}{subsubsection*.9}%
\contentsline {subsubsection}{Step 4. Variables and expressions}{10}{subsubsection*.10}%
\contentsline {subsubsection}{Task completed!}{12}{subsubsection*.11}%
\contentsline {subsection}{\numberline {2.1.5}Using the Collaboration Tool}{12}{subsection.2.1.5}%
\contentsline {subsection}{\numberline {2.1.6}How to get help}{12}{subsection.2.1.6}%
\contentsline {section}{\numberline {2.2}User Guide}{12}{section.2.2}%
\contentsline {subsection}{\numberline {2.2.1}Contents}{12}{subsection.2.2.1}%
\contentsline {subsection}{\numberline {2.2.2}Installation}{13}{subsection.2.2.2}%
\contentsline {subsubsection}{Pre\sphinxhyphen {}requisites}{13}{subsubsection*.12}%
\contentsline {subsubsection}{Install system packages}{13}{subsubsection*.13}%
\contentsline {subsubsection}{Install mldev}{13}{subsubsection*.14}%
\contentsline {subsubsection}{Alternative sources}{13}{subsubsection*.15}%
\contentsline {subsubsection}{Configuration files}{14}{subsubsection*.16}%
\contentsline {subsubsection}{Installing extras}{14}{subsubsection*.17}%
\contentsline {subsection}{\numberline {2.2.3}Experiment setup}{14}{subsection.2.2.3}%
\contentsline {subsubsection}{Step 1. Create a separate project for the experiment}{15}{subsubsection*.18}%
\contentsline {subsubsection}{Step 2. Configure your experiment}{16}{subsubsection*.19}%
\contentsline {subsubsection}{Step 3 \sphinxhyphen {} Commit changes}{16}{subsubsection*.20}%
\contentsline {subsection}{\numberline {2.2.4}Running the experiment (locally)}{16}{subsection.2.2.4}%
\contentsline {subsection}{\numberline {2.2.5}Running the experiment (Google Colab)}{16}{subsection.2.2.5}%
\contentsline {subsection}{\numberline {2.2.6}Advanced usage}{16}{subsection.2.2.6}%
\contentsline {subsubsection}{Using expressions in the experiment configuration}{16}{subsubsection*.21}%
\contentsline {subsubsection}{Using custom types}{17}{subsubsection*.22}%
\contentsline {subsubsection}{Jupyter integration}{18}{subsubsection*.23}%
\contentsline {subsubsection}{Telegram notifications}{18}{subsubsection*.24}%
\contentsline {subsubsection}{Using Tensorboard on Google Colab}{19}{subsubsection*.25}%
\contentsline {subsubsection}{Model demo}{19}{subsubsection*.26}%
\contentsline {subsubsection}{Running pipelines through Gitlab}{19}{subsubsection*.27}%
\contentsline {section}{\numberline {2.3}MLDev collaboration tools (beta)}{19}{section.2.3}%
\contentsline {subsection}{\numberline {2.3.1}Contents}{19}{subsection.2.3.1}%
\contentsline {subsection}{\numberline {2.3.2}Installation}{19}{subsection.2.3.2}%
\contentsline {subsubsection}{Prerequisites}{19}{subsubsection*.28}%
\contentsline {subsubsection}{Install the MLDev extra}{20}{subsubsection*.29}%
\contentsline {subsubsection}{Alternative sources}{20}{subsubsection*.30}%
\contentsline {paragraph}{The Python Package Index (PyPI)}{20}{paragraph*.31}%
\contentsline {paragraph}{The GitLab Package Registry}{20}{paragraph*.32}%
\contentsline {subsection}{\numberline {2.3.3}Initialization}{20}{subsection.2.3.3}%
\contentsline {subsubsection}{Initializing the Tool for a New Experiment}{20}{subsubsection*.33}%
\contentsline {subsubsection}{Initializing the Tool for an Existing Experiment}{21}{subsubsection*.34}%
\contentsline {subsection}{\numberline {2.3.4}Usage}{22}{subsection.2.3.4}%
\contentsline {subsection}{\numberline {2.3.5}Tutorial}{22}{subsection.2.3.5}%
\contentsline {subsubsection}{Step 1. Prepare the Remote Repository}{22}{subsubsection*.35}%
\contentsline {subsubsection}{Step 2. Initialize the Experiment}{22}{subsubsection*.36}%
\contentsline {subsubsection}{Step 3. Make Changes}{24}{subsubsection*.37}%
\contentsline {subsubsection}{Step 4. Simulate the Work of Another Researcher}{25}{subsubsection*.38}%
\contentsline {subsubsection}{Step 5. Merge Your Changes}{25}{subsubsection*.39}%
\contentsline {chapter}{\numberline {3}Developer docs}{29}{chapter.3}%
\contentsline {section}{\numberline {3.1}Contribution Guidelines}{29}{section.3.1}%
\contentsline {subsection}{\numberline {3.1.1}General Guidelines}{29}{subsection.3.1.1}%
\contentsline {subsection}{\numberline {3.1.2}Pre\sphinxhyphen {}requisites}{29}{subsection.3.1.2}%
\contentsline {subsection}{\numberline {3.1.3}Translating and contributing docs and tutorials}{30}{subsection.3.1.3}%
\contentsline {subsection}{\numberline {3.1.4}Reporting bugs and code fixes}{30}{subsection.3.1.4}%
\contentsline {subsection}{\numberline {3.1.5}Joining the project}{30}{subsection.3.1.5}%
\contentsline {section}{\numberline {3.2}Developer documentation}{30}{section.3.2}%
\contentsline {subsection}{\numberline {3.2.1}MLDev Base}{30}{subsection.3.2.1}%
\contentsline {subsection}{\numberline {3.2.2}MLDev Config Parser}{30}{subsection.3.2.2}%
\contentsline {subsection}{\numberline {3.2.3}MLDev Jupyter integration}{30}{subsection.3.2.3}%
\contentsline {subsection}{\numberline {3.2.4}MLDev DVC integration}{30}{subsection.3.2.4}%
\contentsline {subsection}{\numberline {3.2.5}MLDev Collab}{30}{subsection.3.2.5}%
\contentsline {subsubsection}{Collaboration module}{30}{subsubsection*.40}%
\contentsline {subsubsection}{Operations module}{32}{subsubsection*.45}%
\contentsline {chapter}{\numberline {4}Project sponsors and supporters}{35}{chapter.4}%
\contentsline {chapter}{\numberline {5}Contacts}{37}{chapter.5}%
\contentsline {chapter}{\numberline {6}License}{39}{chapter.6}%
\contentsline {chapter}{\numberline {7}Index and tables}{41}{chapter.7}%
\contentsline {chapter}{Содержание модулей Python}{43}{section*.52}%
\contentsline {chapter}{Алфавитный указатель}{45}{section*.53}%
