# Instruction
Here we provide an instruction for using your model with prepared flask controller, provided by our tool
Note that in order to use flask controller you need to have the trained model.

(Instruction is written with assumption that you have already made your own project based on our repository (if not, please use readme.md file of our project)

## Step 1 - experiment.yml configure
* If you want to use your flask controller automatically you need to make a stage of experiment.yml, which will have file with model as an input. (you can see how to configure a stage here: link)
* On the image below you can see typical "demo" stage

![Снимок_экрана_2020-05-17_в_19.48.05](uploads/2391b03403ed4e6b9ee42642825f08b0/Снимок_экрана_2020-05-17_в_19.48.05.png)

* On this stage you can see that as the input file we specified file model.pickle - the file, which contains our trained model

![Снимок_экрана_2020-05-17_в_19.48.05](uploads/4c0b79856a5da72f2ee6ff184f5182cc/Снимок_экрана_2020-05-17_в_19.48.05.png)

* The second essential part of the experiment.yml configuring is that you need to uncomment (or add, if you are making your own stage) the block with the flask controller in the monitoring services section (shown on the image below)

![Снимок_экрана_2020-05-17_в_20.13.48](uploads/c1079d6af86ecf15023d359f02c3dc02/Снимок_экрана_2020-05-17_в_20.13.48.png)

* Please specify ports here (you can use default):

![Снимок_экрана_2020-05-17_в_20.13.48_2](uploads/af945c49ea2c757e4742c2db952713c6/Снимок_экрана_2020-05-17_в_20.13.48_2.png)

* Please enter your Ngrok token here(Instruction for Ngrok token obtaining is here: it is similar to the token for TensorBoard instruction)

![Снимок_экрана_2020-05-17_в_20.13.48_2_2](uploads/00e8abb0fc4e44840de3fa7f5ff7c184/Снимок_экрана_2020-05-17_в_20.13.48_2_2.png)

## Step 2 - Configure Flask controller script
* Open file flaskModelController.py (in the route directory)
* Here you will need to work with three blocks:
* The first block is responsible for the file from where controller will obtain your model and the format of your model, please rewrite it to suit your own case

![Снимок_экрана_2020-05-17_в_20.27.48](uploads/bb351e2723e512c8637688228edc0a1b/Снимок_экрана_2020-05-17_в_20.27.48.png)

* Second block is responsible for configuring work of your model with the specific input data

![Снимок_экрана_2020-05-17_в_20.27.48_2](uploads/c123d86e914c49adfb71270fcc3be7f5/Снимок_экрана_2020-05-17_в_20.27.48_2.png)

* Third block is responsible for Json parsing, if it is not suitable for you as default please rewrite it.

![Снимок_экрана_2020-05-17_в_20.27.48_3](uploads/f18f2682942a232279e838b073758742/Снимок_экрана_2020-05-17_в_20.27.48_3.png)

## Step 3 - Running your model via Flask controller

* After you have successfully completed all the steps you will need to run our instrument with command:

`!cd <experiment repo> && chmod +x ./start_tool.sh && ./start_tool.sh`

* and then open your [Ngrok DashBoard](https://ngrok.io) or add and run in Google Colab this commands:

`!chmod ugo+x ngrok_urls.sh`

`!./ngrok_urls.sh`

**Done! now you can use your model via flaskModelController!**
