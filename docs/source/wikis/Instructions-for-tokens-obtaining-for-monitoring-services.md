# TensorBoard:
## Step 1
*  please visit : [ngrok.com](https://ngrok.com)
## Step 2
*  Sign up there

![Снимок_экрана_2020-05-17_в_17.04.12](uploads/3f8966b50b6d98b47a8b65e6b18a559d/Снимок_экрана_2020-05-17_в_17.04.12.png)

## Step 3

* After you have successfully signed up go to the authentication tab in the right side bar
* Then tap to the "Your Authtoken"
* Copy your token from the bar at the top of the page

![Снимок_экрана_2020-05-17_в_17.45.44](uploads/d31763bf3aee3b7c1c69e0a52736b7c4/Снимок_экрана_2020-05-17_в_17.45.44.png)
![Снимок_экрана_2020-05-17_в_17.46.00](uploads/d5cb7bce252c224f72e37ef13ae04ebf/Снимок_экрана_2020-05-17_в_17.46.00.png)
![Снимок_экрана_2020-05-17_в_17.46.08](uploads/436fc38433ce269de35c6066a1bb79c2/Снимок_экрана_2020-05-17_в_17.46.08.png)

* **Done! You Successfully obtained your token for Ngrok!**
* **Now you can paste it to the field shown below in experiment.yml**

![Снимок_экрана_2020-05-14_в_09.04.20](uploads/2f2d70aeebd33cf071c61c1cc70acc05/Снимок_экрана_2020-05-14_в_09.04.20.png)

# Telegram Bot
## Step1
* Visit [telegram](https://web.telegram.org)
## Step2
* Sign in there
## Step3 
* Find bot with name `@BotFather` here is link for you: [BotFather](https://t.me/BotFather)
* run command `/start`
* run command `/newbot`
* give your new bot a name 
* after that you will receive a massage with the token you need
* **Done! You Successfully obtained your token for notification Bot!**
* **Now you can paste it to the field shown below in experiment.yml**

![Снимок_экрана_2020-05-14_в_09.04.20](uploads/9322f37b2aad7f98470df937413b6c2e/Снимок_экрана_2020-05-14_в_09.04.20.png)