## What is Mldev Templates?

Mldev Templates are templates which can be used to properly organize your project directory. You can find all available templates [here](https://gitlab.com/mlrep). This templates can help you in your work and also will be useful for publication of your experiment results.

## How to use Mldev Templates?

In order to use one of the templates you need to run the following command:

`mldev init -t <chosen_template> <your_project_folder>`

If you have any troubles choosing one of the templates you can omit that parameter, and it will be set to the ``template-default``.

By default, ``mldev`` initializes a new repo for the template. 
This can be disabled using the ``--no-commit`` switch. 
You can also reuse already present folder or repo by using the ``-r`` switch. The ``-t`` option will be ignored.
There are other options for ``mldev init``, run ``mldev --help`` to get a full list.