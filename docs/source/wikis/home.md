# Документация MLDev

- [User guide](./mldev-user-guide)
- [Quickstart Tutorial](./mldev-tutorial-basic)
- [Mldev-Templates](https://gitlab.com/mlrep/mldev/-/wikis/Mldev-Templates)
- [Configure experiment.yml](./configure-experiment.yml)
- [Flask Controller](./flask-controller-instructions)
- [Instructions fo obtaining tokens](./instructions-for-tokens-obtaining-for-monitoring-services)

## Development notes

- [Как проводить эксперименты](./Как-проводить-эксперименты)
- [Which yaml parser to use?](./which-yaml-parser-to-use)
- [Синтез конфигураций](./синтез-конфигураций)
- [Что такое подготовленный эксперимент](./что-есть-подготовленный-эксперимент)

## Older docs

- [Installation Instruction](https://gitlab.com/mlrep/mldev/-/blob/38-documentation-issue/README.md)