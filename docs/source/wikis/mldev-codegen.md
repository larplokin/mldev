# mldev-codegen

Эта страница посвящена mldev-codegen проекту предназначенному для генерации запускаемого python скрипта из yml файла. Проект доступен [здесь](https://gitlab.com/mlrep/mldev-codegen), актуальная версия в ветке [develop](https://gitlab.com/mlrep/mldev-codegen/-/tree/develop). 

Генерация скриптов основана на использовании jinja2 шаблонов. Код генерируется только для объектов BasicStage класса. 

### Структура проекта

Проект имеет следующую структуру:
- yaml_examples: директория содержащая примеры yml файлов
- templates: директория содержащая jinja2 шаблоны
- generated_scripts: директория собержащая скрипты, сгенерированные на основе файлов из директории yaml_examples
- expanded_script_generator.py: скрпит, генерирующий python скрипт по заданному yml файлу и сохраняющий его
- main.py: скрипт, использующий expanded_script_generator.py для всех файлов из yaml_examples
- README

### Пайплайн генерации кода

1. yml файл считывается с помощью `mldev.yaml_loader.YamlLoaderWithEnvVars`. Засчет этого также поддерживается использование Expression.
2. Из описания эксперимента отбираются объекты класса BasicStage, которые определены в корне либо к секции runs объекта типа GenericPipeline который определен в корне.
   - то есть если у GenericPipeline в секции runs определяется другой GenericPipeline, у которого в секции runs определяется BasicStage, для этого стейджа код сгенерирован не будет.
   - Naming convention: для стейджей, определенных в пайплайнах, генерируются функции с именем `<stage_name>_IN_<pipeline_name>`. 
3. Для каждого BasicStage объекта генерируется словарь, описывающий его (функция `cast_stage_params_to_strings`). Это аналогично методу `__repr__` класса `BasicStage`, но вдобавок:
    - все элементы типа `mldev.expression.Expression` трансформируются в строку.
    - все элементы типа `mldev.experiment.FilePath` трансформируются в пути к соответствующим файлам (возможна трансформация в абсолютные или относительные пути).
4. Набор словарей для всех стейджей передается в jinja2 шаблон для генерации скрипта.

### jinja2 шаблон

Используемый [jinja2 шаблон](https://gitlab.com/mlrep/mldev-codegen/-/blob/develop/template/GenericPipeline_tmpl.py) состоит из следующих частей:

1. import необходимых пактов
2. Определение функций соответствующих BasicStage объектам. Каждый объект описывается следующими параметрами: `name, params, env, inputs, outputs, script`:
   - `name` - используется в качестве имени функции
   - `params` - набор параметров функции
   - `env` - описание venv: словарь в теле функции
   - `inputs` - список в теле функции
   - `outputs` - список в теле функции 
   - `script` - список в теле функции

Выполнение функции происходит аналогично [методу](https://gitlab.com/mlrep/mldev/-/blob/develop/src/mldev/experiment.py#L127) `.run()` для объектов класса BasicStage

3. main: парсинг названия стейджа, который нужно запустить и параметров, которые передаются в форме json string. После этого идет вызов функции соответствующей переданному названию. 

Если указано некорректное имя стейджа, будет выведено сообщение: `Unknown BasicStage: <stage_name>`.

### Использование 

Для использования сгенерированного скрипта:

1. Переместить скрипт в директорию эксперимента
1. `python <script_name>.py <stage_name> --params '<json_string_with_params>'`

### Тестирование

Сгенерированный скрипт был протестирован для [template-default эксперимента](https://gitlab.com/mlrep/template-default) ([сгенерированный скрипт](https://gitlab.com/mlrep/mldev-codegen/-/blob/develop/generated_scripts/experiment_basic.py)). Способ запуска:

```
python experiment_basic.py prepare_IN_run_prepare
python experiment_basic.py train_IN_run_prepare_train --params '{"num_iters":2}'
python experiment_basic.py predict_IN_run_predict
```

### Текущие проблемы

1. В текущей реализации параметры BasicStage записываются в том виде, в котором их возвращает `mldev.yaml_loader.YamlLoaderWithEnvVars`, поэтому если внутри встречаются `mldev.expression.Expression`, то они будут отображаться некорректно. Это связано с тем что отлавливать `Expression` в параметрах сложнее, чем в других полях(`inputs`, `outputs`, `script`), потому что в отличие от них может представлять собой сложную структуру из вложенных словарей.
   - как решить: запустить обход словаря params как дерева и конвертировать к строке каждый лист
2. Если скрипт стейджа имеет вид
```
python ./src/main.py single-model \
               --model_params "${json(self.params.model_params)}" \
               --params "${json(self.params.experiment)}" \
               --folder "${self.outputs[0].path}" \
               --random_seed 42 \
               --run_times 10
```
то возникнет конфликт кавычек при хранении этой строки в скрипте. Строка приобретает следующий вид:
```
script = ['python ./src/main.py single-model \n       
               --model_params "{"gbr_model": {"n_estimators": 150, "max_depth": 3, "criterion": "mae", "loss": "huber"}, "ridge_model": {}}" \n       
               --params "{"train_size": 0.3}" \n       
               --folder "./results/single_model" \n
               --random_seed 42 \n       --run_times 10'
]
```
для которой после вызывается `prepare_experiment_command`. Скобки, обрамляющие json string параметров смешиваются с остальными скобками и не могут корректно конвертированы в json.
   - как решить: непонятно. Возможно использование тройных скобок может помочь.