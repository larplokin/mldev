# Here we provide some steps to help you with experiment.yml configuration:

## Step1

* Open experiment.yml from any of the templates provided [here](https://gitlab.com/mlrep) 
(for example: [from here](https://gitlab.com/mlrep/template-default/-/blob/master/experiment.yml)) in your favorite editor for .yml files

## Step2

* Please take a look on the file and follow the next steps in order to configure 
the file in accordance with the desired behavior

## Step3

* In experiment.yml you can see enumeration of two stages (Prepare, Train),
 List of Monitoring services and Stages themselves. 
 We will discuss all the file structure and features starting from top to bottom.

## Step4

* The first field **STAGES** is responsible for the stages enumeration. 
Stage is an independent part of your experiment 
(it may be data preparing, training of your model, evaluation or any other.) 
Here you need to specify names of all stages that you will use (in our example we have to stages: prepare and train)

![Снимок_экрана_2020-09-17_в_18.39.34](uploads/a0d50bbf4be629574c9592b89db30d06/Снимок_экрана_2020-09-17_в_18.39.34.png)

# Monitoring services

### Telegram bot: 
Telegram bot is used to obtain notifications about your experiment procedure via telegram

### Tensorboard:
Gives you an opportunity to obtain metrics and graphs during the process of your experiment 
(for more information please visit: [tenosrboard](https://www.tensorflow.org/tensorboard).

### Ngrok
is a special service for tunneling (is used for model controller and tensorboard). 
For more information please visit: [ngrok](https://ngrok.com)

## Step5

after you have enumerated your stages you will need to configure monitoring services 
that will be used in your experiment 
(recall that here you need to specify settings for all monitoring services, 
without minding the stage where they will be used.). 

Monitoring services are the services which will help your to monitor experiment move. 
Now we provide four services, they are: Tensorboard, Telegram bot, ngrok, and model controller. 
For all of them you will need to obtain your personal tokens, 
in order to do that please visit: [Tokens](https://gitlab.com/mlrep/mldev/-/wikis/Instructions-for-tokens-obtaining-for-monitoring-services)

~~please mind that you need to use "&" before each name of the service.~~

for every service except the model controller you will need to type your personal 
token in the the appropriate field. 

For model controller you will need to specify file with your model with the path to it 
(please see the image below).

For all services except telegram bot you need to specify port that will be used for 
this service. For telegram bot you may specify warnings field (with true or false).  

For tensorboard please specify directory and file for logs writng. 
Mind that you need to configure ngrok if you would like to use tensorboard.

![Снимок_экрана_2020-09-17_в_18.39.42](uploads/2ffc1c1ed3565b05cc0158d95e4b982e/Снимок_экрана_2020-09-17_в_18.39.42.png)

# Stages

## Step6

After we have set monitoring services we can move to the stages themselves 
(mind that you can specify stages earlier and then move to the instruments 
if it is more suitable for you.) In the example file you can see two stages, 
we will discuss one of them, because configuring others is actually the same process. 

![Снимок_экрана_2020-09-17_в_18.39.55](uploads/b621a0c44f673bd15810fff648bd86ab/Снимок_экрана_2020-09-17_в_18.39.55.png)

for every stage you need to configure this parameters (attributes):
* needs_dvc: shows wether you need dvc support on this stage or not 
(to learn what dvc is please visit this [page](dvc.org))
* monitoring: specify here services that wiil be used in this particular stage
* versioned_input_data: here you need to specify files which will be added as 
an input dependencies for this stage. You can leave this field blank, 
provide directory, or specify List of the input files. 
(please see the example on the image above in the field versioned_output_data, 
the behavior is exactly the same)
* versioned_output_data: this parameter is the same as input, only difference 
is that it is used to configure output dependencies.
* script: here you need to provide script which will be executed on this stage.
* please mind that you need to use "*" symbol before monitoring services names.

# Conditions and Loops

## Step7 - Conditions

in experiment.yml you can use conditions and loops in order to reduce the amount 
of configured stages, or perfom an experiment with the changing parameter.

in order to use a condition you need to introduce it like shown on the image below:

![Снимок_экрана_2020-09-17_в_19.30.00](uploads/ae0d7402a5ef1eb9b82ac8fa31eb10bb/Снимок_экрана_2020-09-17_в_19.30.00.png)

in the field statement please provide boolean condition. 
If the statement is true stages specified under the trueCase will be executed, 
in other case stages under FalseCase will be executed. 

![Снимок_экрана_2020-09-17_в_19.35.06](uploads/fbf919c9378baed7c16cb79fa9df40ca/Снимок_экрана_2020-09-17_в_19.35.06.png)

## Step8 - Loops

experiment.yml gives you an ability to use loop forEach, where you can pass a 
Collection and function, and this function will be executed on every element 
of this collection.

![Снимок_экрана_2020-09-17_в_19.37.18](uploads/c484a755677bac1e793082edeb361174/Снимок_экрана_2020-09-17_в_19.37.18.png)

in this example we provide collection of stages (they are specified under 
the field collections), and command "-echo iterating", which will be executed 
for every stage in the collection.

# Variables

## Step9 - Env variables
You can use env variables in experiment.yml to provide sensitive data 
(passwords, tokens etc). Type 

```export ENV_NAME=ENV_VALUE```

in the same terminal from which you are going to run the tool. 
Vars are stored in /home/{user}/.config/mldev/ between runs, 
that's no need to provide same variables any time you run the tool.  

![ngrok_token_env](uploads/aa35244480d0a221b56d84547483fad7/ngrok_token_env.jpg)

* Note that you can use any amount of the stages
* Note that the names of your stages *must be different*, and that different stages *can not have same input files*
 